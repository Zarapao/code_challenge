import csv
import pathlib
class FindPathMain:

    def setData(self,fileName):
        graphArr = []
        graph, graphGoal = {},{}
        # with open('graph.csv') as csvfile:
        with open(fileName) as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                graphArr.append(row)
                
            graphArr2 = graphArr.copy()
            for row in graphArr:
                a,b = row[0],row[1]
                if row[0] in graph or row[0] == "0":
                    continue
                else:
                    graphGoal[row[1]] = row[2]
                    for row2 in graphArr2:
                        if row[0] in row2:
                            if row[0] != row2[0]:
                                graphGoal[row2[0]] = row2[2]
                            else:
                                graphGoal[row2[1]] = row2[2]
                    graph[row[0]] = graphGoal
                    graphGoal = {} 
                
                if row[1] in graph  or row[1] == "0":
                    continue
                else:
                    graphGoal[row[0]] = row[2]
                    for row2 in graphArr2:
                        if row[1] in row2:
                            if row[1] != row2[1]:
                                graphGoal[row2[1]] = row2[2]
                            else:
                                graphGoal[row2[0]] = row2[2]
                    graph[row[1]] = graphGoal
                    graphGoal = {}
        return graph   

    def findPath(self,fileName,start,goal):
        
        if start == goal:
            return "Start and goal is same."
        checkFile = fileName.split(".")
        if checkFile[len(checkFile)-1] != "csv":
            return "Not .csv file."

        file = pathlib.Path(fileName)
        if not file.exists():
            return "File not exist."

        graph = self.setData(fileName)
        path,costArr,tempPath= [],[],[]
        cost = 0
        start = start.upper()
        goal = goal.upper()

        def findPathAll(path,start,costArr,cost):
            if start in path:
                return

            costArr.append(int(cost))
            path.append(start)

            if start == goal:
                tempPath.append([path,sum(costArr)])
                return

            for run in graph[start]:
                findPathAll(path.copy(),run,costArr.copy(),graph[start][run])

        findPathAll(path,start,costArr,cost)

        if len(tempPath) == 0:
            return "Path not exist."

        for run in tempPath:
            if cost == 0:
                cost = int(run[1])
                tempPath = run[0]
            if cost > run[1]:
                cost = int(run[1])
                tempPath = run[0]

            outPath=""                  
            for a in tempPath:
                outPath += (outPath!="") and ("->" +a) or a
        return f"Path from {start} to {goal} is {outPath} and have cost {cost}."

if __name__ == '__main__':
    findPathMain = FindPathMain()
    fileName = input("What is graph file name: ")
    start = input("What is start node: ").upper()
    goal = input("What is goal node: ").upper()
    print(findPathMain.findPath(fileName,start,goal))