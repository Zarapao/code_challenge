import unittest
import code_challenge as Code

class TestFindPath(unittest.TestCase):
    codeFindPath = Code.FindPathMain()

    def test_fild_path_a_to_b(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","A","B"), "Path from A to B is A->B and have cost 5.")

    def test_fild_path_b_to_a(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","B","A"), "Path from B to A is B->A and have cost 5.")
    
    def test_fild_path_b_to_a_lower(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","b","a"), "Path from B to A is B->A and have cost 5.")
    
    def test_fild_path_a_to_c(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","A","C"), "Path from A to C is A->B->C and have cost 9.")
    
    def test_fild_path_c_to_a(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","C","A"), "Path from C to A is C->B->A and have cost 9.")
    
    def test_fild_path_c_to_f(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","C","F"), "Path from C to F is C->G->H->F and have cost 10.")
    
    def test_fild_path_f_to_g(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","F","G"), "Path from F to G is F->H->G and have cost 8.")

    def test_fild_path_f_to_c(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","F","C"), "Path from F to C is F->H->G->C and have cost 10.")
    
    def test_fild_path_b_to_h(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","B","H"), "Path from B to H is B->C->G->H and have cost 9.")
    
    def test_file_not_csv(self):
        self.assertEqual(self.codeFindPath.findPath("graph.py","B","H"), "Not .csv file.")
    
    def test_fild_path_a_to_a(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","A","A"), "Start and goal is same.")

    def test_fild_path_a_to_i(self):
        self.assertEqual(self.codeFindPath.findPath("graph.csv","A","I"), "Path not exist.")
    
    def test_fild_not_exist(self):
        self.assertEqual(self.codeFindPath.findPath("graph22.csv","A","C"), "File not exist.")


if __name__ == "__main__":
    unittest.main()